from django.contrib import admin
from django.contrib.auth.models import Group
from home.models import Contact
from home.models import Product, offer, Cookies, Cakeandpastries


class ContactAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'desc', 'phone')
    list_per_page = 10
    search_fields = ('name', 'email', 'desc', 'phone')
    list_filter = ["date"]


class offeradmin(admin.ModelAdmin):
    list_display = ('code', 'description', 'discount')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'stock', 'image_url')


class CookiesAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'stock', 'image_url')


class CakeandpastriesAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'stock', 'image_url')


admin.site.register(Contact, ContactAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(offer, offeradmin)
admin.site.register(Cookies, CookiesAdmin)
admin.site.register(Cakeandpastries, CakeandpastriesAdmin)
admin.site.unregister(Group)