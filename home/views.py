from django.shortcuts import render, HttpResponse
from datetime import datetime
from home.models import Contact
from django.contrib import messages
from .models import Product, Cookies, Cakeandpastries


# Create your views here.
def index(request):
    products = Product.objects.all()
    return render(request, 'index.html', {"products": products})


def about(request):
    return render(request, 'about.html')


def services(request):
    cookies = Cookies.objects.all()
    return render(request, 'services.html', {"cookies": cookies})


def cakesandpastries(request):
    cakeandpastries = Cakeandpastries.objects.all()
    return render(request, 'cakesandpastries.html', {"cakeandpastries": cakeandpastries})


def contact(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        desc = request.POST.get('desc')
        contact = Contact(name=name, email=email, phone=phone, desc=desc, date=datetime.today())
        contact.save()
        messages.success(request, 'Your message submitted successfully')
    return render(request, 'contact.html')
